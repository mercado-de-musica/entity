package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "contrato_correios")
public class ContratoCorreios implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)				
	private BigInteger id;
	
	@Column(name = "codigo_administrativo_correios", length = 8)
	private String codigoAdministrativoCorreios;
	
	@Column(name = "usuario", nullable = false)
	private String usuario;
	
	@Column(name = "senha", nullable = false, insertable = false, updatable = false)
	private String senha;
	
	@Column(name = "id_diretoria", nullable = false)
	private String idDiretoria;
	
	@Column(name = "id_cartao_postagem", nullable = false)
	private String idCartaoPostagem;
	
	@Column(name = "numero_do_contrato", nullable = false)
	private String numeroDoContrato;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonManagedReference
	private Set<NumeroServicosCorreios> arrayNumerosServicosCorreios;
	
	@Column(name = "ativo", nullable = false)
	private Boolean ativo;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getCodigoAdministrativoCorreios() {
		return codigoAdministrativoCorreios;
	}

	public void setCodigoAdministrativoCorreios(String codigoAdministrativoCorreios) {
		this.codigoAdministrativoCorreios = codigoAdministrativoCorreios;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getIdDiretoria() {
		return idDiretoria;
	}

	public void setIdDiretoria(String idDiretoria) {
		this.idDiretoria = idDiretoria;
	}

	public String getIdCartaoPostagem() {
		return idCartaoPostagem;
	}

	public void setIdCartaoPostagem(String idCartaoPostagem) {
		this.idCartaoPostagem = idCartaoPostagem;
	}

	public String getNumeroDoContrato() {
		return numeroDoContrato;
	}

	public void setNumeroDoContrato(String numeroDoContrato) {
		this.numeroDoContrato = numeroDoContrato;
	}

	public Set<NumeroServicosCorreios> getArrayNumerosServicosCorreios() {
		return arrayNumerosServicosCorreios;
	}

	public void setArrayNumerosServicosCorreios(Set<NumeroServicosCorreios> arrayNumerosServicosCorreios) {
		this.arrayNumerosServicosCorreios = arrayNumerosServicosCorreios;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
