package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.mercadodemusica.enums.InstituicaoEnum;

@Entity
@Table(name = "compra_cartao_de_credito")
public class CompraCartaoDeCredito implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1399985638236233164L;
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(name = "instituicao")
	@Enumerated(EnumType.STRING)
	private InstituicaoEnum instituicao;
	
	@Column(name = "primeiros_numeros", length = 20)
	private String primeirosNumeros;
	
	@Column(name = "ultimos_numeros", length = 20)
	private String ultimosNumeros;
	
	@Column(name = "id_pagamento", length = 100)
	private String idPagamento;
	
	@JoinColumn(name="id_compra", referencedColumnName = "id", nullable=true, columnDefinition="bigint")
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = true)
	private Compra compra;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public InstituicaoEnum getInstituicao() {
		return instituicao;
	}

	public void setInstituicao(InstituicaoEnum instituicao) {
		this.instituicao = instituicao;
	}
	
	public String getPrimeirosNumeros() {
		return primeirosNumeros;
	}

	public void setPrimeirosNumeros(String primeirosNumeros) {
		this.primeirosNumeros = primeirosNumeros;
	}

	public String getUltimosNumeros() {
		return ultimosNumeros;
	}

	public void setUltimosNumeros(String ultimosNumeros) {
		this.ultimosNumeros = ultimosNumeros;
	}

	public String getIdPagamento() {
		return idPagamento;
	}

	public void setIdPagamento(String idPagamento) {
		this.idPagamento = idPagamento;
	}

	public Compra getCompra() {
		return compra;
	}

	public void setCompra(Compra compra) {
		this.compra = compra;
	}
}
