package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.mercadodemusica.enums.DiaDaSemana;

@Entity
@Table(name = "task_schedulada", schema = "crawler")
public class TaskSchedulada implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition = "serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Column(name = "hora_de_disparo")
	private Integer horaDeDisparo;
	
	@Column(name = "minuto_de_disparo")
	private Integer minutoDeDisparo;

	@Enumerated(EnumType.STRING)
	@Column(name = "dia_da_semana")
	private DiaDaSemana diaDaSemana;

	private Boolean executando;

	private Boolean ativo;
	
	@Column(name = "random_nome", nullable = true)
	private String randomNome;

	@OneToOne
	@JoinColumn(name = "id_usuario", referencedColumnName = "id")
	private Usuario usuario;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Integer getHoraDeDisparo() {
		return horaDeDisparo;
	}

	public void setHoraDeDisparo(Integer horaDeDisparo) {
		this.horaDeDisparo = horaDeDisparo;
	}

	public Integer getMinutoDeDisparo() {
		return minutoDeDisparo;
	}

	public void setMinutoDeDisparo(Integer minutoDeDisparo) {
		this.minutoDeDisparo = minutoDeDisparo;
	}

	public DiaDaSemana getDiaDaSemana() {
		return diaDaSemana;
	}

	public void setDiaDaSemana(DiaDaSemana diaDaSemana) {
		this.diaDaSemana = diaDaSemana;
	}

	public Boolean getExecutando() {
		return executando;
	}

	public void setExecutando(Boolean executando) {
		this.executando = executando;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getRandomNome() {
		return randomNome;
	}

	public void setRandomNome(String randomNome) {
		this.randomNome = randomNome;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
