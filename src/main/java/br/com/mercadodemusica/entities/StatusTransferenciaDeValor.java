package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.mercadodemusica.enums.StatusTransferenciaDeValoresGatewayDePagamentoEnum;

@Entity
@Table(name = "status_transferencia_valor")
public class StatusTransferenciaDeValor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3360217759847537196L;
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(name = "status_gateway_pagamento", nullable = false)
	@Enumerated(EnumType.STRING)
	private StatusTransferenciaDeValoresGatewayDePagamentoEnum statusTransferenciaDeValoresGatewayDePagamento;
	
	@Column(name = "data_transferencia", nullable = false)
	private Calendar dataTransferencia;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "id_transferencia_valores_usuario", referencedColumnName = "id", nullable = false, columnDefinition="bigint")
	@JsonBackReference
	private TransferenciaValoresUsuario transferenciaValoresUsuario;
	
	@Column(name = "ativo", nullable = false)
	private Boolean ativo = Boolean.FALSE;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public StatusTransferenciaDeValoresGatewayDePagamentoEnum getStatusTransferenciaDeValoresGatewayDePagamento() {
		return statusTransferenciaDeValoresGatewayDePagamento;
	}

	public void setStatusTransferenciaDeValoresGatewayDePagamento(
			StatusTransferenciaDeValoresGatewayDePagamentoEnum statusTransferenciaDeValoresGatewayDePagamento) {
		this.statusTransferenciaDeValoresGatewayDePagamento = statusTransferenciaDeValoresGatewayDePagamento;
	}

	public Calendar getDataTransferencia() {
		return dataTransferencia;
	}

	public void setDataTransferencia(Calendar dataTransferencia) {
		this.dataTransferencia = dataTransferencia;
	}

	public TransferenciaValoresUsuario getTransferenciaValoresUsuario() {
		return transferenciaValoresUsuario;
	}

	public void setTransferenciaValoresUsuario(TransferenciaValoresUsuario transferenciaValoresUsuario) {
		this.transferenciaValoresUsuario = transferenciaValoresUsuario;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
