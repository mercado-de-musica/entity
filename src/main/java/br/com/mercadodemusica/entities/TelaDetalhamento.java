package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tela_detalhamento", schema = "crawler")
public class TelaDetalhamento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(columnDefinition="text", nullable = false)
	private String telaDetalhamento;
	
	@Column(length = 300, nullable = false, name="url_acesso")
	private String urlAcesso;
	
	@Column(length = 300, nullable = false, name = "base_url")
	private String baseUrl;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getTelaDetalhamento() {
		return telaDetalhamento;
	}

	public void setTelaDetalhamento(String telaDetalhamento) {
		this.telaDetalhamento = telaDetalhamento;
	}

	public String getUrlAcesso() {
		return urlAcesso;
	}

	public void setUrlAcesso(String urlAcesso) {
		this.urlAcesso = urlAcesso;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
}
