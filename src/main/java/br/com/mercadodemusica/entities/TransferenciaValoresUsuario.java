package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "transferencia_valores_usuario")
public class TransferenciaValoresUsuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
//	@JsonManagedReference
	@OneToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.EAGER)	
	private List<Produto> listaProduto;
	
	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "usuario")
	private Usuario usuario;
	
	@Column(name = "finalizado", nullable = false)
	private Boolean finalizado = Boolean.FALSE;
	
	@Column(name = "id_transferencia_gateway_pagamento", nullable = true)
	private String transferenciaGatewayDePagamento;
	
	@Column(name = "valor_transferido", nullable = true)
	private BigDecimal valorTransferido;
	
	@Column(name="porcentagem", nullable = false)
	private BigDecimal porcentagem = BigDecimal.ZERO;
		
	@OneToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "conta_usada_transferencia", nullable = true)
	private ContaBancaria contaUsadaNaTransferencia;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "transferenciaValoresUsuario")
	@Fetch(value = FetchMode.SUBSELECT)
	@JsonManagedReference
	private List<StatusTransferenciaDeValor> listaStatusTransferenciaDeValor;
	
	@Column(name = "desistencia", nullable = false)
	private Boolean desistencia = Boolean.FALSE;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public List<Produto> getListaProduto() {
		return listaProduto;
	}

	public void setListaProduto(List<Produto> listaProduto) {
		this.listaProduto = listaProduto;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	public String getTransferenciaGatewayDePagamento() {
		return transferenciaGatewayDePagamento;
	}

	public void setTransferenciaGatewayDePagamento(String transferenciaGatewayDePagamento) {
		this.transferenciaGatewayDePagamento = transferenciaGatewayDePagamento;
	}

	public BigDecimal getValorTransferido() {
		return valorTransferido;
	}

	public void setValorTransferido(BigDecimal valorTransferido) {
		this.valorTransferido = valorTransferido;
	}

	public BigDecimal getPorcentagem() {
		return porcentagem;
	}

	public void setPorcentagem(BigDecimal porcentagem) {
		this.porcentagem = porcentagem;
	}

	public ContaBancaria getContaUsadaNaTransferencia() {
		return contaUsadaNaTransferencia;
	}

	public void setContaUsadaNaTransferencia(ContaBancaria contaUsadaNaTransferencia) {
		this.contaUsadaNaTransferencia = contaUsadaNaTransferencia;
	}

	public List<StatusTransferenciaDeValor> getListaStatusTransferenciaDeValor() {
		return listaStatusTransferenciaDeValor;
	}

	public void setListaStatusTransferenciaDeValor(List<StatusTransferenciaDeValor> listaStatusTransferenciaDeValor) {
		this.listaStatusTransferenciaDeValor = listaStatusTransferenciaDeValor;
	}

	public Boolean getDesistencia() {
		return desistencia;
	}

	public void setDesistencia(Boolean desistencia) {
		this.desistencia = desistencia;
	}
}