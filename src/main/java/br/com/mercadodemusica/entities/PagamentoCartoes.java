package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.mercadodemusica.enums.InstituicaoEnum;
import br.com.mercadodemusica.enums.TipoCartaoEnum;

@Entity
@Table(name = "pagamento_cartoes")
public class PagamentoCartoes implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Enumerated(EnumType.STRING)
	private TipoCartaoEnum tipoCartao;
	
	@Enumerated(EnumType.STRING)
	private InstituicaoEnum instituicao;
	
	@Column(name = "cofre", length = 60, nullable = true)
	private String cofre;
	
	@JoinColumn(name="id_usuario")
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Usuario usuario;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public TipoCartaoEnum getTipoCartao() {
		return tipoCartao;
	}

	public void setTipoCartao(TipoCartaoEnum tipoCartao) {
		this.tipoCartao = tipoCartao;
	}

	public InstituicaoEnum getInstituicao() {
		return instituicao;
	}

	public void setInstituicao(InstituicaoEnum instituicao) {
		this.instituicao = instituicao;
	}

	public String getCofre() {
		return cofre;
	}

	public void setCofre(String cofre) {
		this.cofre = cofre;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
