package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "numero_servicos_correios")
public class NumeroServicosCorreios implements Serializable{

	/**
	 * Tb_Servicos_ECT
	 * para saber se tem sedex 10, normal, etc.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(name = "id_servico", unique = true, nullable = false)
	private Long idServico;
	
	@Column(name = "numero_servico", unique = true, nullable = false)
	private String numeroServico;
	
	@Column(name = "nome_servico", nullable = false)
	private String nomeServico;
	
	@Column(name = "descricao", nullable = true)
	private String descricao;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_contrato_correios", referencedColumnName = "id")
	@JsonBackReference
	private ContratoCorreios contratoCorreios;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Long getIdServico() {
		return idServico;
	}

	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}

	public String getNumeroServico() {
		return numeroServico;
	}

	public void setNumeroServico(String numeroServico) {
		this.numeroServico = numeroServico;
	}

	public String getNomeServico() {
		return nomeServico;
	}

	public void setNomeServico(String nomeServico) {
		this.nomeServico = nomeServico;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public ContratoCorreios getContratoCorreios() {
		return contratoCorreios;
	}

	public void setContratoCorreios(ContratoCorreios contratoCorreios) {
		this.contratoCorreios = contratoCorreios;
	}
}
