package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pergunta_infos_gerais_produto")
public class PerguntaCategoriaProduto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(nullable = false, columnDefinition="text")
	private String pergunta;
	
	@Column(name = "data_pergunta", nullable = false)
	private Calendar dataPergunta;
	
	@JoinColumn(name="id_infos_gerais_produto_usuario")
	@OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	private Produto infosGeraisProdutoUsuario;
	
	@Column(name = "id_pai", nullable = true, columnDefinition="bigint")
	private BigInteger idPai;
	
	@JoinColumn(name = "id_usuario_interessado")
	@OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private Usuario usuarioInteressado;
	
	@Column(name = "enviado_email")
	private Boolean enviadoEmail = Boolean.FALSE;
	
	@Column(name = "data_envio_email")
	private Calendar dataEnvioEmail;
	
	@Column(name = "respondido", nullable = false)
	private Boolean respondido = Boolean.FALSE;
	
	@Column(name = "encerrar_discussao", nullable = false)
	private Boolean encerrarDiscussao = Boolean.FALSE;
	
	@Column(name = "visualizar_pelo_comprador", nullable = false, columnDefinition = "boolean default true")
	private Boolean visualizarPeloComprador = Boolean.TRUE;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getPergunta() {
		return pergunta;
	}

	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}

	public Calendar getDataPergunta() {
		return dataPergunta;
	}

	public void setDataPergunta(Calendar dataPergunta) {
		this.dataPergunta = dataPergunta;
	}

	public Produto getProduto() {
		return infosGeraisProdutoUsuario;
	}

	public void setProduto(Produto infosGeraisProdutoUsuario) {
		this.infosGeraisProdutoUsuario = infosGeraisProdutoUsuario;
	}

	public BigInteger getIdPai() {
		return idPai;
	}

	public void setIdPai(BigInteger idPai) {
		this.idPai = idPai;
	}

	public Usuario getUsuarioInteressado() {
		return usuarioInteressado;
	}

	public void setUsuarioInteressado(Usuario usuarioInteressado) {
		this.usuarioInteressado = usuarioInteressado;
	}

	public Boolean getEnviadoEmail() {
		return enviadoEmail;
	}

	public void setEnviadoEmail(Boolean enviadoEmail) {
		this.enviadoEmail = enviadoEmail;
	}

	public Calendar getDataEnvioEmail() {
		return dataEnvioEmail;
	}

	public void setDataEnvioEmail(Calendar dataEnvioEmail) {
		this.dataEnvioEmail = dataEnvioEmail;
	}

	public Boolean getRespondido() {
		return respondido;
	}

	public void setRespondido(Boolean respondido) {
		this.respondido = respondido;
	}

	public Boolean getEncerrarDiscussao() {
		return encerrarDiscussao;
	}

	public void setEncerrarDiscussao(Boolean encerrarDiscussao) {
		this.encerrarDiscussao = encerrarDiscussao;
	}

	public Boolean getVisualizarPeloComprador() {
		return visualizarPeloComprador;
	}

	public void setVisualizarPeloComprador(Boolean visualizarPeloComprador) {
		this.visualizarPeloComprador = visualizarPeloComprador;
	}
}
