package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.mercadodemusica.enums.BancoEnum;

@Entity
@Table(name = "conta_bancaria")
public class ContaBancaria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2267414283373372356L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "numero_banco", nullable = false, length = 4)
	private BancoEnum numeroDoBanco;
	
	@Column(name = "numero_agencia", nullable = false, length = 5)
	private String numeroDaAgencia;
	
	@Column(name = "digito_agencia", nullable = true, length = 2)
	private String digitoDaAgencia;
	
	@Column(name = "numero_conta", nullable = false, length = 20)
	private String numeroDaConta;
	
	@Column(name = "digito_conta", nullable = false, length = 2)
	private String digitoDaConta;
	
	@Column(name = "nome_da_conta", nullable = true)
	private String nomeDaConta;
	
	@JoinColumn(name="id_usuario")
	@OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.EAGER)
	private Usuario usuario;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BancoEnum getNumeroDoBanco() {
		return numeroDoBanco;
	}

	public void setNumeroDoBanco(BancoEnum numeroDoBanco) {
		this.numeroDoBanco = numeroDoBanco;
	}

	public String getNumeroDaAgencia() {
		return numeroDaAgencia;
	}

	public void setNumeroDaAgencia(String numeroDaAgencia) {
		this.numeroDaAgencia = numeroDaAgencia;
	}

	public String getDigitoDaAgencia() {
		return digitoDaAgencia;
	}

	public void setDigitoDaAgencia(String digitoDaAgencia) {
		this.digitoDaAgencia = digitoDaAgencia;
	}

	public String getNumeroDaConta() {
		return numeroDaConta;
	}

	public void setNumeroDaConta(String numeroDaConta) {
		this.numeroDaConta = numeroDaConta;
	}

	public String getDigitoDaConta() {
		return digitoDaConta;
	}

	public void setDigitoDaConta(String digitoDaConta) {
		this.digitoDaConta = digitoDaConta;
	}

	public String getNomeDaConta() {
		return nomeDaConta;
	}

	public void setNomeDaConta(String nomeDaConta) {
		this.nomeDaConta = nomeDaConta;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
