package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="compra_boleto")
public class CompraBoleto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6084583509109316713L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(name = "id_pagamento", length = 100)
	private String idPagamento;
	
	@Column(name = "vencimento_boleto")
	private Calendar vencimentoBoleto;
	
	@Column(name = "url_boleto", nullable = false)
	private String urlBoleto;
	
	@JoinColumn(name="id_compra", referencedColumnName = "id", nullable=true, columnDefinition="bigint")
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = true)
	private Compra compra;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getIdPagamento() {
		return idPagamento;
	}

	public void setIdPagamento(String idPagamento) {
		this.idPagamento = idPagamento;
	}

	public Calendar getVencimentoBoleto() {
		return vencimentoBoleto;
	}

	public void setVencimentoBoleto(Calendar vencimentoBoleto) {
		this.vencimentoBoleto = vencimentoBoleto;
	}

	public String getUrlBoleto() {
		return urlBoleto;
	}

	public void setUrlBoleto(String urlBoleto) {
		this.urlBoleto = urlBoleto;
	}

	public Compra getCompra() {
		return compra;
	}

	public void setCompra(Compra compra) {
		this.compra = compra;
	}
}
