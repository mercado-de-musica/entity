package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "parcelamento_de_valor")
public class ParcelamentoDeValor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8392903925004631542L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(name ="vezes_parceladas", columnDefinition="bigint", nullable = false)
	private BigInteger vezesParceladas;
	
	@Column(name = "taxa", nullable = false)
	private BigDecimal taxa;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getVezesParceladas() {
		return vezesParceladas;
	}

	public void setVezesParceladas(BigInteger vezesParceladas) {
		this.vezesParceladas = vezesParceladas;
	}

	public BigDecimal getTaxa() {
		return taxa;
	}

	public void setTaxa(BigDecimal taxa) {
		this.taxa = taxa;
	}
}
