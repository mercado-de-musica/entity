package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.mercadodemusica.enums.PaisesEnum;

@Entity
@Table(name = "pais")
public class Pais implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Enumerated(EnumType.STRING)
	@Column(name="nome", length = 80)
	private PaisesEnum paisEnum;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public PaisesEnum getPaisEnum() {
		return paisEnum;
	}

	public void setPaisEnum(PaisesEnum paisEnum) {
		this.paisEnum = paisEnum;
	}
}
