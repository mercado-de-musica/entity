package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "token_recuperacao_senha")
public class TokenRecuperacaoSenha implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -830937707531326737L;
	
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@ManyToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
	private Usuario usuario;
	
	@Column(name = "token", unique = true)
	private String token; 
	
	@Column(name = "ativo")
	private Boolean ativo;
	
	@Column(name = "data_recuperacao_senha")
	private Calendar dataRecuperacaoSenha;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Calendar getDataRecuperacaoSenha() {
		return dataRecuperacaoSenha;
	}

	public void setDataRecuperacaoSenha(Calendar dataRecuperacaoSenha) {
		this.dataRecuperacaoSenha = dataRecuperacaoSenha;
	}
}
