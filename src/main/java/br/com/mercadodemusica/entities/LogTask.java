package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.mercadodemusica.enums.FinalizacaoTask;

@Entity
@Table(name = "log_task", schema = "crawler")
public class LogTask implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition = "serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Column(name = "finalizacao_task")
	@Enumerated(EnumType.STRING)
	private FinalizacaoTask finalizacaoTask;

	@Column(name = "descricao_finalizacao_task")
	private String descricaoFinalizacaoTask;

	@Column(name = "data_finalizacao")
	private Calendar dataFinalizacao;

	@OneToOne
	@JoinColumn(name = "id_task_schedulada", referencedColumnName = "id")
	private TaskSchedulada taskSchedulada;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public FinalizacaoTask getFinalizacaoTask() {
		return finalizacaoTask;
	}

	public void setFinalizacaoTask(FinalizacaoTask finalizacaoTask) {
		this.finalizacaoTask = finalizacaoTask;
	}

	public String getDescricaoFinalizacaoTask() {
		return descricaoFinalizacaoTask;
	}

	public void setDescricaoFinalizacaoTask(String descricaoFinalizacaoTask) {
		this.descricaoFinalizacaoTask = descricaoFinalizacaoTask;
	}

	public Calendar getDataFinalizacao() {
		return dataFinalizacao;
	}

	public void setDataFinalizacao(Calendar dataFinalizacao) {
		this.dataFinalizacao = dataFinalizacao;
	}

	public TaskSchedulada getTaskSchedulada() {
		return taskSchedulada;
	}

	public void setTaskSchedulada(TaskSchedulada taskSchedulada) {
		this.taskSchedulada = taskSchedulada;
	}
}
