package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.mercadodemusica.enums.MotivoTagEnum;
import br.com.mercadodemusica.enums.TipoTagEnum;

@Entity
@Table(name = "tag", schema = "crawler")
public class Tag implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(columnDefinition = "serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Column(nullable = false)
	private String tag;

	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_tag", nullable = false)
	private TipoTagEnum tipoTag;

	@Enumerated(EnumType.STRING)
	@Column(name = "motivo_tag", nullable = false)
	private MotivoTagEnum motivoTag;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "id_usuario", referencedColumnName = "id")
	private Usuario usuario;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public TipoTagEnum getTipoTag() {
		return tipoTag;
	}

	public void setTipoTag(TipoTagEnum tipoTag) {
		this.tipoTag = tipoTag;
	}

	public MotivoTagEnum getMotivoTag() {
		return motivoTag;
	}

	public void setMotivoTag(MotivoTagEnum motivoTag) {
		this.motivoTag = motivoTag;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
