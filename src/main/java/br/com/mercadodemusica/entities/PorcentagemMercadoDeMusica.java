package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "porcentagem_mercado_de_musica")
public class PorcentagemMercadoDeMusica implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5929255735519299723L;
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(name="porcentagem", nullable = false)
	private BigDecimal porcentagem;
	
	@Column(name="data_de_insercao", nullable = false)
	private Calendar dataDeInsercao;
	
	@Column(name="data_de_mudanca_no_sistema", nullable = true)
	private Calendar dataDeMudancaNoSistema;
	
	@JoinColumn(name="id_usuario", nullable = false)
	@OneToOne(fetch = FetchType.EAGER)
	private Usuario usuario;
	
	@JoinColumn(name="ativo", nullable = false)
	private Boolean ativo;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigDecimal getPorcentagem() {
		return porcentagem;
	}

	public void setPorcentagem(BigDecimal porcentagem) {
		this.porcentagem = porcentagem;
	}

	public Calendar getDataDeInsercao() {
		return dataDeInsercao;
	}

	public void setDataDeInsercao(Calendar dataDeInsercao) {
		this.dataDeInsercao = dataDeInsercao;
	}

	public Calendar getDataDeMudancaNoSistema() {
		return dataDeMudancaNoSistema;
	}

	public void setDataDeMudancaNoSistema(Calendar dataDeMudancaNoSistema) {
		this.dataDeMudancaNoSistema = dataDeMudancaNoSistema;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
