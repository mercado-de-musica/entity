package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.mercadodemusica.enums.TipoCartaoEnum;

@Entity
@Table(name = "custo_gateway_pagamento")
public class CustoGatewayPagamento implements Serializable {	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2479622863832139838L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(name="tipo_de_pagamento", nullable = false)
	@Enumerated(EnumType.STRING)
	private TipoCartaoEnum tipoDePagamento;
	
	@Column(name="porcentagem", nullable = false)
	private BigDecimal porcentagem;
	
	@Column(name="custo_transacao", nullable = false)
	private BigDecimal custoTransacao;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public TipoCartaoEnum getTipoDePagamento() {
		return tipoDePagamento;
	}

	public void setTipoDePagamento(TipoCartaoEnum tipoDePagamento) {
		this.tipoDePagamento = tipoDePagamento;
	}

	public BigDecimal getPorcentagem() {
		return porcentagem;
	}

	public void setPorcentagem(BigDecimal porcentagem) {
		this.porcentagem = porcentagem;
	}

	public BigDecimal getCustoTransacao() {
		return custoTransacao;
	}

	public void setCustoTransacao(BigDecimal custoTransacao) {
		this.custoTransacao = custoTransacao;
	}
}
