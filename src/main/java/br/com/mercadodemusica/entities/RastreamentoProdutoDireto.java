package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "rastreamento_produto_direto")
public class RastreamentoProdutoDireto extends Rastreamento implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="codigo_comprador", nullable = false)
	private String codigoComprador;
	
	@Column(name="codigo_vendedor", nullable = false)
	private String codigoVendedor;

	@Column(name="data_uso_do_codigo", nullable = true)
	private Calendar dataUsoDoCodigo;
	
	@Column(name="codigo_inserido_comprador", nullable = true)
	private String codigoInseridoComprador;
	
	@Column(name="codigo_inserido_vendedor", nullable = true)
	private String codigoInseridoVendedor;
	
	@Column(name="data_insercao_codigo_comprador", nullable = true)
	private Calendar dataInsercaoCodigoComprador;
	
	@Column(name="data_insercao_codigo_vendedor", nullable = true)
	private Calendar dataInsercaoCodigoVendedor;
	
	public String getCodigoComprador() {
		return codigoComprador;
	}

	public void setCodigoComprador(String codigoComprador) {
		this.codigoComprador = codigoComprador;
	}

	public String getCodigoVendedor() {
		return codigoVendedor;
	}

	public void setCodigoVendedor(String codigoVendedor) {
		this.codigoVendedor = codigoVendedor;
	}

	public Calendar getDataUsoDoCodigo() {
		return dataUsoDoCodigo;
	}

	public void setDataUsoDoCodigo(Calendar dataUsoDoCodigo) {
		this.dataUsoDoCodigo = dataUsoDoCodigo;
	}

	public String getCodigoInseridoComprador() {
		return codigoInseridoComprador;
	}

	public void setCodigoInseridoComprador(String codigoInseridoComprador) {
		this.codigoInseridoComprador = codigoInseridoComprador;
	}

	public String getCodigoInseridoVendedor() {
		return codigoInseridoVendedor;
	}

	public void setCodigoInseridoVendedor(String codigoInseridoVendedor) {
		this.codigoInseridoVendedor = codigoInseridoVendedor;
	}

	public Calendar getDataInsercaoCodigoComprador() {
		return dataInsercaoCodigoComprador;
	}

	public void setDataInsercaoCodigoComprador(Calendar dataInsercaoCodigoComprador) {
		this.dataInsercaoCodigoComprador = dataInsercaoCodigoComprador;
	}

	public Calendar getDataInsercaoCodigoVendedor() {
		return dataInsercaoCodigoVendedor;
	}

	public void setDataInsercaoCodigoVendedor(Calendar dataInsercaoCodigoVendedor) {
		this.dataInsercaoCodigoVendedor = dataInsercaoCodigoVendedor;
	}
}
