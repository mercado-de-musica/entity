package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.mercadodemusica.enums.IntermediadorDePagamentoEnum;
import br.com.mercadodemusica.enums.StatusPagamentoEnum;
import br.com.mercadodemusica.enums.StatusTransacaoEnum;
import br.com.mercadodemusica.enums.TipoPagamentoEnum;
@Entity
@Table(name = "compra")
public class Compra implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)		
	private BigInteger id;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_pagamento")
	private TipoPagamentoEnum tipoPagamento;
	
	@Column(name = "status_pagamento", nullable = true)
	@Enumerated(EnumType.STRING)
	private StatusPagamentoEnum statusPagamento;
	
	@Column(name = "status_transacao", nullable = true)
	@Enumerated(EnumType.STRING)
	private StatusTransacaoEnum statusTransacao;
	
	@Column(name = "id_pedido", length = 100, nullable = true)
	private String idPedido;
	
	@Column(name = "token_pedido", length = 200, nullable = true)
	private String tokenPedido;
	
	@Column(name = "mensagem")
	private String mensagem;
	
	@Column(name = "total_pago")
	private BigDecimal totalPago;
	
	@Column(name = "parcelas")
	private Integer parcelas;

	@Column(name="aceite_politica_de_compra")
	private Boolean aceitePoliticaDeCompra;

//	@Column(name = "riscos_da_transacao")
//	private Boolean riscosDaTransacao;
	
	@Column(name = "finalizado", nullable = false)
	private Boolean finalizado = Boolean.FALSE;
	
	@Column(name = "criacao", nullable = false)
	private Calendar criacao = Calendar.getInstance();
	
	@Column(name = "intermediador_de_pagamento", nullable = true)
	@Enumerated(EnumType.STRING)
	private IntermediadorDePagamentoEnum intermediadorDePagamento;
	
	@Column(nullable = true, name ="id_comprador")
	private BigInteger idComprador;
	
	@Column(nullable = true, name ="ip_do_comprador")
	private String ipComprador;
	
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public TipoPagamentoEnum getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public StatusPagamentoEnum getStatusPagamento() {
		return statusPagamento;
	}

	public void setStatusPagamento(StatusPagamentoEnum statusPagamento) {
		this.statusPagamento = statusPagamento;
	}

	public StatusTransacaoEnum getStatusTransacao() {
		return statusTransacao;
	}

	public void setStatusTransacao(StatusTransacaoEnum statusTransacao) {
		this.statusTransacao = statusTransacao;
	}
	
	public String getIdPedido() {
		return idPedido;
	}

	public void setIdPedido(String idPedido) {
		this.idPedido = idPedido;
	}

	public String getTokenPedido() {
		return tokenPedido;
	}

	public void setTokenPedido(String tokenPedido) {
		this.tokenPedido = tokenPedido;
	}

	public String getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public BigDecimal getTotalPago() {
		return totalPago;
	}

	public void setTotalPago(BigDecimal totalPago) {
		this.totalPago = totalPago;
	}

	public Integer getParcelas() {
		return parcelas;
	}

	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}

	public Boolean getAceitePoliticaDeCompra() {
		return aceitePoliticaDeCompra;
	}

	public void setAceitePoliticaDeCompra(Boolean aceitePoliticaDeCompra) {
		this.aceitePoliticaDeCompra = aceitePoliticaDeCompra;
	}

//	public List<RiscosDaTransacao> getRiscosDaTransacao() {
//		return riscosDaTransacao;
//	}
//
//	public void setRiscosDaTransacao(List<RiscosDaTransacao> riscosDaTransacao) {
//		this.riscosDaTransacao = riscosDaTransacao;
//	}

	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	public Calendar getCriacao() {
		return criacao;
	}

	public void setCriacao(Calendar criacao) {
		this.criacao = criacao;
	}

	public IntermediadorDePagamentoEnum getIntermediadorDePagamento() {
		return intermediadorDePagamento;
	}

	public void setIntermediadorDePagamento(IntermediadorDePagamentoEnum intermediadorDePagamento) {
		this.intermediadorDePagamento = intermediadorDePagamento;
	}

	public BigInteger getIdComprador() {
		return idComprador;
	}

	public void setIdComprador(BigInteger idComprador) {
		this.idComprador = idComprador;
	}

	public String getIpComprador() {
		return ipComprador;
	}

	public void setIpComprador(String ipComprador) {
		this.ipComprador = ipComprador;
	}
}
