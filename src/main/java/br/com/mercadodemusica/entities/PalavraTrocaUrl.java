package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.mercadodemusica.enums.PalavraTrocaUrlEnum;

@Entity
@Table(name = "palavra_troca_url", schema = "crawler")
public class PalavraTrocaUrl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition = "serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Enumerated(EnumType.STRING)
	@Column(name = "palavra_troca_url_enum")
	private PalavraTrocaUrlEnum palavraTrocaUrlEnum;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "id_parametro_paginacao", referencedColumnName = "id")
	private ParametroPaginacao parametroPaginacao;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public PalavraTrocaUrlEnum getPalavraTrocaUrlEnum() {
		return palavraTrocaUrlEnum;
	}

	public void setPalavraTrocaUrlEnum(PalavraTrocaUrlEnum palavraTrocaUrlEnum) {
		this.palavraTrocaUrlEnum = palavraTrocaUrlEnum;
	}

	public ParametroPaginacao getParametroPaginacao() {
		return parametroPaginacao;
	}

	public void setParametroPaginacao(ParametroPaginacao parametroPaginacao) {
		this.parametroPaginacao = parametroPaginacao;
	}
}
