package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.mercadodemusica.enums.DistanciaInteresseDeCompraEnum;

@Entity
@Table(name = "interesses_de_compra")
public class InteressesDeCompra implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(name = "id_instrumento_acessorio", nullable = true, columnDefinition="bigint")
	private BigInteger instrumentoAcessorio;
	
	@Column(name = "id_marca", nullable = true, columnDefinition="bigint")
	private BigInteger marca;

	@Column(name = "id_produto", nullable = true, columnDefinition="bigint")
	private BigInteger produto;
	
	@Column(name = "id_modelo", nullable = true, columnDefinition="bigint")
	private BigInteger modelo;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
	private Usuario usuario;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "distancia")
	private DistanciaInteresseDeCompraEnum distancia;
    
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getInstrumentoAcessorio() {
		return instrumentoAcessorio;
	}

	public void setInstrumentoAcessorio(BigInteger instrumentoAcessorio) {
		this.instrumentoAcessorio = instrumentoAcessorio;
	}

	public BigInteger getMarca() {
		return marca;
	}

	public void setMarca(BigInteger marca) {
		this.marca = marca;
	}

	public BigInteger getProduto() {
		return produto;
	}

	public void setProduto(BigInteger produto) {
		this.produto = produto;
	}

	public BigInteger getModelo() {
		return modelo;
	}

	public void setModelo(BigInteger modelo) {
		this.modelo = modelo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public DistanciaInteresseDeCompraEnum getDistancia() {
		return distancia;
	}

	public void setDistancia(DistanciaInteresseDeCompraEnum distancia) {
		this.distancia = distancia;
	}
}
