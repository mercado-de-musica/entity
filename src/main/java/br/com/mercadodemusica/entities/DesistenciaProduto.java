package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "desistencia_de_produto")
public class DesistenciaProduto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2317608489166825337L;
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="id_rastreamento", nullable = false, referencedColumnName = "id")
	private Rastreamento rastreamento;
	
	@Column(name = "data_do_processamento")
	private Calendar dataDoProcessamento;
	
	@Column(name="finalizado", nullable = false)
	private Boolean finalizado = Boolean.FALSE;
	
	@Column(name="protocolo_desistencia_compra", nullable = false)
	private String protocoloDesistenciaDeCompra;
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Rastreamento getRastreamento() {
		return rastreamento;
	}

	public void setRastreamento(Rastreamento rastreamento) {
		this.rastreamento = rastreamento;
	}

	public Calendar getDataDoProcessamento() {
		return dataDoProcessamento;
	}

	public void setDataDoProcessamento(Calendar dataDoProcessamento) {
		this.dataDoProcessamento = dataDoProcessamento;
	}

	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	public String getProtocoloDesistenciaDeCompra() {
		return protocoloDesistenciaDeCompra;
	}

	public void setProtocoloDesistenciaDeCompra(String protocoloDesistenciaDeCompra) {
		this.protocoloDesistenciaDeCompra = protocoloDesistenciaDeCompra;
	}
}
