package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "historico_datas_de_compra")
public class HistoricoDatasDeCompra implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8732957214618062306L;
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)		
	private BigInteger id;
	
	@Column(name = "data_da_compra")
	private Calendar dataDaCompra;
	
	@Column(name = "data_atualizada")
	private Boolean dataAtualizada = Boolean.FALSE;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_compra", referencedColumnName = "id", columnDefinition="bigint")
	@JsonBackReference
	private Compra compra;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Calendar getDataDaCompra() {
		return dataDaCompra;
	}

	public void setDataDaCompra(Calendar dataDaCompra) {
		this.dataDaCompra = dataDaCompra;
	}

	public Boolean getDataAtualizada() {
		return dataAtualizada;
	}

	public void setDataAtualizada(Boolean dataAtualizada) {
		this.dataAtualizada = dataAtualizada;
	}

	public Compra getCompra() {
		return compra;
	}

	public void setCompra(Compra compra) {
		this.compra = compra;
	}
}
