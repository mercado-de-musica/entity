package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.mercadodemusica.enums.TipoDeRequisicaoDoSiteEnum;

@Entity
@Table(name = "usuario_crawler")
public class UsuarioCrawler extends Usuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(length = 40, nullable = false)
	private String nome;

	@Column(nullable = false)
	private String url;

	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_procura_site_enum")
	private TipoDeRequisicaoDoSiteEnum tipoDeProcuraDoSiteEnum;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "usuario")
	private List<PalavraChaveUsuario> palavrasChavesUsuarios;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public TipoDeRequisicaoDoSiteEnum getTipoDeProcuraDoSiteEnum() {
		return tipoDeProcuraDoSiteEnum;
	}

	public void setTipoDeProcuraDoSiteEnum(TipoDeRequisicaoDoSiteEnum tipoDeProcuraDoSiteEnum) {
		this.tipoDeProcuraDoSiteEnum = tipoDeProcuraDoSiteEnum;
	}

	public List<PalavraChaveUsuario> getPalavrasChavesUsuarios() {
		return palavrasChavesUsuarios;
	}

	public void setPalavrasChavesUsuarios(List<PalavraChaveUsuario> palavrasChavesUsuarios) {
		this.palavrasChavesUsuarios = palavrasChavesUsuarios;
	}
}
