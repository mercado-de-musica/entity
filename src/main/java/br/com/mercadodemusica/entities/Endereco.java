package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;


@Entity
@Table(name = "endereco")
public class Endereco implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(name = "nome_do_endereco", length = 30, nullable = true)
	private String nomeDoEndereco;
	
	@Column(length = 70, nullable = false)
	private String logradouro;
	
	@Column(length = 10, nullable = false)
	private String numero;
	
	@Column(length = 8, nullable = false)
	private String cep;
	
	@Column(length = 60, nullable = true)
	private String complemento;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name="id_cidade")
	private Cidade cidade;
	
	@Column(length = 60, nullable = false)
	private String bairro;
	
	@Column(length = 60, nullable = true)
	private String proximidade;
	
	@Column(name = "endereco_default", nullable = true)
	private Boolean enderecoDefault;
	
	@Column(name = "latitude", nullable = false, precision = 31, scale = 20)
	private BigDecimal latitude;
	
	@Column(name = "longitude", nullable = false, precision = 31, scale = 20)
	private BigDecimal longitude;
	
	@Column(name = "posicao", columnDefinition = "geometry", nullable = false)
	@Type(type = "org.hibernate.spatial.GeometryType")
	@JsonBackReference
	private Point posicao = new Point(null, new GeometryFactory());
	
	@ManyToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
	private Usuario usuario;
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getNomeDoEndereco() {
		return nomeDoEndereco;
	}

	public void setNomeDoEndereco(String nomeDoEndereco) {
		this.nomeDoEndereco = nomeDoEndereco;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getProximidade() {
		return proximidade;
	}

	public void setProximidade(String proximidade) {
		this.proximidade = proximidade;
	}

	public Boolean getEnderecoDefault() {
		return enderecoDefault;
	}

	public void setEnderecoDefault(Boolean enderecoDefault) {
		this.enderecoDefault = enderecoDefault;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public Point getPosicao() {
		return posicao;
	}

	public void setPosicao(Point posicao) {
		this.posicao = posicao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
