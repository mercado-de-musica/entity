package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "valor_frete_correio")
public class ValorFreteCorreio implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2220929329942561335L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(name = "valor")
	private BigDecimal valor;
	
	@Column(name = "id_servico_correio", nullable = false)
	private Long idServicoCorreio;
	
	@Column(name = "codigo_correio", nullable = false)
	private String codigoCorreio;
	
	@Column(name = "descricao_correio")
	private String descricaoCorreio;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "id_infos_gerais_produto_usuario", referencedColumnName = "id", nullable=true, columnDefinition="bigint")
	@JsonBackReference
	private Produto infosGeraisProdutoUsuario;

	@Column(name = "dias_entrega")
	private String diasEntrega;
	
	@Column(name = "ativo_para_compra", nullable = false)
	private Boolean ativoParaCompra;
	
	@Column(name = "data_atualizacao")
	private Calendar dataAtualizacao;
	
	/*
	 * atributo utilizado para fazer os merges de dados apenas
	 */
	
	@Column(name = "ativo_merge")
	@Transient
	private Boolean ativoMerge;
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Long getIdServicoCorreio() {
		return idServicoCorreio;
	}

	public void setIdServicoCorreio(Long idServicoCorreio) {
		this.idServicoCorreio = idServicoCorreio;
	}

	public String getCodigoCorreio() {
		return codigoCorreio;
	}

	public void setCodigoCorreio(String codigoCorreio) {
		this.codigoCorreio = codigoCorreio;
	}

	public String getDescricaoCorreio() {
		return descricaoCorreio;
	}

	public void setDescricaoCorreio(String descricaoCorreio) {
		this.descricaoCorreio = descricaoCorreio;
	}

	public Produto getProduto() {
		return infosGeraisProdutoUsuario;
	}

	public void setProduto(Produto infosGeraisProdutoUsuario) {
		this.infosGeraisProdutoUsuario = infosGeraisProdutoUsuario;
	}

	public String getDiasEntrega() {
		return diasEntrega;
	}

	public void setDiasEntrega(String diasEntrega) {
		this.diasEntrega = diasEntrega;
	}

	public Boolean getAtivoParaCompra() {
		return ativoParaCompra;
	}

	public void setAtivoParaCompra(Boolean ativoParaCompra) {
		this.ativoParaCompra = ativoParaCompra;
	}

	public Calendar getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(Calendar dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public Boolean getAtivoMerge() {
		return ativoMerge;
	}

	public void setAtivoMerge(Boolean ativoMerge) {
		this.ativoMerge = ativoMerge;
	}
}
