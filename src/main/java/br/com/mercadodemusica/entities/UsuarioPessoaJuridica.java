package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "usuario_pessoa_juridica")
public class UsuarioPessoaJuridica extends Usuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "razao_social", length = 100, nullable = false)
	private String razaoSocial;
	
	@Column(name = "nome_fantasia", length = 50, nullable = false)
	private String nomeFantasia;
	
	@Column(length = 14, nullable = false, unique = true)
	private String cnpj;
		
	@Column(length = 50, nullable = false, name="nome_titular")
	private String nomeTitular;
	
	@Column(length = 50, nullable = false, name="sobrenome_titular")
	private String sobrenomeTitular;
	
	@Column(name = "data_de_nascimento_titular", nullable = false)
	private Calendar dataDeNascimentoTitular;
	
	@Column(length = 20, nullable = false, name="rg_titular")
	private String rgTitular;
	
	@Column(length = 11, nullable = false, unique = true, name="cpf_titular")
	private String cpfTitular;
	
//	@Column(nullable = false, name="orgao_expedidor")
//	private String orgaoExpedidor;
//	
//	@Column(nullable = false, name="data_expedicao")
//	private Calendar dataExpedicao;
	
	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNomeTitular() {
		return nomeTitular;
	}

	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}

	public String getSobrenomeTitular() {
		return sobrenomeTitular;
	}

	public void setSobrenomeTitular(String sobrenomeTitular) {
		this.sobrenomeTitular = sobrenomeTitular;
	}

	public Calendar getDataDeNascimentoTitular() {
		return dataDeNascimentoTitular;
	}

	public void setDataDeNascimentoTitular(Calendar dataDeNascimentoTitular) {
		this.dataDeNascimentoTitular = dataDeNascimentoTitular;
	}

	public String getRgTitular() {
		return rgTitular;
	}

	public void setRgTitular(String rgTitular) {
		this.rgTitular = rgTitular;
	}

	public String getCpfTitular() {
		return cpfTitular;
	}

	public void setCpfTitular(String cpfTitular) {
		this.cpfTitular = cpfTitular;
	}

//	public String getOrgaoExpedidor() {
//		return orgaoExpedidor;
//	}
//
//	public void setOrgaoExpedidor(String orgaoExpedidor) {
//		this.orgaoExpedidor = orgaoExpedidor;
//	}
//
//	public Calendar getDataExpedicao() {
//		return dataExpedicao;
//	}
//
//	public void setDataExpedicao(Calendar dataExpedicao) {
//		this.dataExpedicao = dataExpedicao;
//	}
}
