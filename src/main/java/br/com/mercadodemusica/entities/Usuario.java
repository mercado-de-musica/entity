package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.mercadodemusica.enums.TipoUsuarioEnum;

@Entity
@Table(name = "usuario")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Usuario implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Enumerated(EnumType.STRING)
	@Column(name="tipo_usuario", length = 30)
	private TipoUsuarioEnum tipoUsuario;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JsonManagedReference
	private Set<Telefone> telefones;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JsonManagedReference
	private Set<Endereco> enderecos;

//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	@JsonManagedReference
//	private Set<InteressesDeCompra> interessesDeCompra;
	
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	@JsonManagedReference
//	private Set<LikeProduto> listaDeLikes;
	
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	@JsonManagedReference
//	private List<ReputacaoVendedor> reputacaoVendedor;
	
	private Boolean ativo;
	
	@Column(name="data_de_cadastramento", nullable = false)
	private Calendar dataDeCadastramento;
	
	@Column(name="data_de_update", nullable = false)
	private Calendar dataDeUpdate = Calendar.getInstance();

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public TipoUsuarioEnum getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuarioEnum tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public Set<Telefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(Set<Telefone> telefones) {
		this.telefones = telefones;
	}

	public Set<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(Set<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

//	public Set<InteressesDeCompra> getInteressesDeCompra() {
//		return interessesDeCompra;
//	}
//
//	public void setInteressesDeCompra(Set<InteressesDeCompra> interessesDeCompra) {
//		this.interessesDeCompra = interessesDeCompra;
//	}

//	public Set<LikeProduto> getListaDeLikes() {
//		return listaDeLikes;
//	}
//
//	public void setListaDeLikes(Set<LikeProduto> listaDeLikes) {
//		this.listaDeLikes = listaDeLikes;
//	}
//
//	public List<ReputacaoVendedor> getReputacaoVendedor() {
//		return reputacaoVendedor;
//	}
//
//	public void setReputacaoVendedor(List<ReputacaoVendedor> reputacaoVendedor) {
//		this.reputacaoVendedor = reputacaoVendedor;
//	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	/**
	 * @return the dataDeCadastramento
	 */
	public Calendar getDataDeCadastramento() {
		return dataDeCadastramento;
	}

	/**
	 * @param dataDeCadastramento the dataDeCadastramento to set
	 */
	public void setDataDeCadastramento(Calendar dataDeCadastramento) {
		this.dataDeCadastramento = dataDeCadastramento;
	}

	/**
	 * @return the dataDeUpdate
	 */
	public Calendar getDataDeUpdate() {
		return dataDeUpdate;
	}

	/**
	 * @param dataDeUpdate the dataDeUpdate to set
	 */
	public void setDataDeUpdate(Calendar dataDeUpdate) {
		this.dataDeUpdate = dataDeUpdate;
	}
}
