package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sugestao_novo_produto")
public class SugestaoNovoProduto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(name="sugestao", length = 100)
	private String sugestao;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Column(name="id_infos_gerais_produto", nullable = true, columnDefinition = "bigint")
	private BigInteger infosGeraisProduto;
	
	@Column(name="acatado")
	private Boolean acatado;
	
	@Column(name="ativo")
	private Boolean ativo;
	
	@Column(name="data_de_entrada_da_sugestao")
	private Calendar dataDeEntradaDaSugestao;
	
	@Column(name="data_de_verificacao_da_sugestao")
	private Calendar dataDeVerificacaoDaSugestao;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getSugestao() {
		return sugestao;
	}

	public void setSugestao(String sugestao) {
		this.sugestao = sugestao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public BigInteger getCategoriaProduto() {
		return infosGeraisProduto;
	}

	public void setCategoriaProduto(BigInteger infosGeraisProduto) {
		this.infosGeraisProduto = infosGeraisProduto;
	}

	public Boolean getAcatado() {
		return acatado;
	}

	public void setAcatado(Boolean acatado) {
		this.acatado = acatado;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Calendar getDataDeEntradaDaSugestao() {
		return dataDeEntradaDaSugestao;
	}

	public void setDataDeEntradaDaSugestao(Calendar dataDeEntradaDaSugestao) {
		this.dataDeEntradaDaSugestao = dataDeEntradaDaSugestao;
	}

	public Calendar getDataDeVerificacaoDaSugestao() {
		return dataDeVerificacaoDaSugestao;
	}

	public void setDataDeVerificacaoDaSugestao(Calendar dataDeVerificacaoDaSugestao) {
		this.dataDeVerificacaoDaSugestao = dataDeVerificacaoDaSugestao;
	}
	
}
