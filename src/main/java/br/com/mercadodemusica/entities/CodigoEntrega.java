package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "codigo_entrega")
public class CodigoEntrega implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6901777476105905902L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(name="codigo_comprador", nullable = false)
	private String codigoComprador;
	
	@Column(name="codigo_vendedor", nullable = false)
	private String codigoVendedor;
	
	@Column(name="data_geracao_codigo", nullable = false)
	private Calendar dataGeracaoCodigo;
	
	@Column(name="codigo_utilizado")
	private Boolean codigoUtilizado = Boolean.FALSE;
	
	@Column(name="codigo_inserido_comprador", nullable = true)
	private String codigoInseridoComprador;
	
	@Column(name="codigo_inserido_vendedor", nullable = true)
	private String codigoInseridoVendedor;
	
	@Column(name="data_insercao_codigo_comprador", nullable = true)
	private Calendar dataInsercaoCodigoComprador;
	
	@Column(name="data_insercao_codigo_vendedor", nullable = true)
	private Calendar dataInsercaoCodigoVendedor;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getCodigoComprador() {
		return codigoComprador;
	}

	public void setCodigoComprador(String codigoComprador) {
		this.codigoComprador = codigoComprador;
	}

	public String getCodigoVendedor() {
		return codigoVendedor;
	}

	public void setCodigoVendedor(String codigoVendedor) {
		this.codigoVendedor = codigoVendedor;
	}

	public Calendar getDataGeracaoCodigo() {
		return dataGeracaoCodigo;
	}

	public void setDataGeracaoCodigo(Calendar dataGeracaoCodigo) {
		this.dataGeracaoCodigo = dataGeracaoCodigo;
	}

	public Boolean getCodigoUtilizado() {
		return codigoUtilizado;
	}

	public void setCodigoUtilizado(Boolean codigoUtilizado) {
		this.codigoUtilizado = codigoUtilizado;
	}

	public String getCodigoInseridoComprador() {
		return codigoInseridoComprador;
	}

	public void setCodigoInseridoComprador(String codigoInseridoComprador) {
		this.codigoInseridoComprador = codigoInseridoComprador;
	}

	public String getCodigoInseridoVendedor() {
		return codigoInseridoVendedor;
	}

	public void setCodigoInseridoVendedor(String codigoInseridoVendedor) {
		this.codigoInseridoVendedor = codigoInseridoVendedor;
	}

	public Calendar getDataInsercaoCodigoComprador() {
		return dataInsercaoCodigoComprador;
	}

	public void setDataInsercaoCodigoComprador(Calendar dataInsercaoCodigoComprador) {
		this.dataInsercaoCodigoComprador = dataInsercaoCodigoComprador;
	}

	public Calendar getDataInsercaoCodigoVendedor() {
		return dataInsercaoCodigoVendedor;
	}

	public void setDataInsercaoCodigoVendedor(Calendar dataInsercaoCodigoVendedor) {
		this.dataInsercaoCodigoVendedor = dataInsercaoCodigoVendedor;
	}
	
}
