package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mapeamento_pesquisa_detalhe")
public class MapeamentoPesquisaDetalhe implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(length = 25, nullable = false)
	private String ip;
	
	@Column(nullable = false)
	private String pesquisa;
	
	@Column(nullable = false, name = "id_produto_crypt")
	private String idProdutoCrypt;
	
	private Calendar data;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPesquisa() {
		return pesquisa;
	}

	public void setPesquisa(String pesquisa) {
		this.pesquisa = pesquisa;
	}

	public String getIdProdutoCrypt() {
		return idProdutoCrypt;
	}

	public void setIdProdutoCrypt(String idProdutoCrypt) {
		this.idProdutoCrypt = idProdutoCrypt;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}
}
