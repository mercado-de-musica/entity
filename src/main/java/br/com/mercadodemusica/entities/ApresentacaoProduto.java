package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.mercadodemusica.enums.TipoApresentacaoEnum;

@Entity
@Table(name = "apresentacao_produto")
public class ApresentacaoProduto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(length = 400, nullable = false)
	private String endereco;
	
	@Column(nullable = false)
	private Boolean thumb;
	
	@Column(name="tipo_apresentacao", nullable = false)
	@Enumerated(EnumType.STRING)
	private TipoApresentacaoEnum tipoApresentacao;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_produto")
    private Produto produto;
	
	@Column(nullable = true)
	private BigDecimal scale;
	
	@Column(nullable = true, columnDefinition="bigint")
    private BigInteger width;
    
	@Column(nullable = true, columnDefinition="bigint")
    private BigInteger height;
    
	@Column(nullable = true, columnDefinition="bigint")
    private BigInteger angle;
    
	@Column(nullable = true, columnDefinition="bigint")
    private BigInteger x;
    
	@Column(nullable = true, columnDefinition="bigint")
    private BigInteger y;
	
	@Column(name = "tipo_do_arquivo", length = 10, nullable = true)
    private String tipoArquivo;
	
	@Column(name = "ajuste", nullable = false)
	private Boolean ajuste = false;
		
	@Column(name = "capa", nullable = false)
	private Boolean capa = Boolean.FALSE;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Boolean getThumb() {
		return thumb;
	}

	public void setThumb(Boolean thumb) {
		this.thumb = thumb;
	}

	public TipoApresentacaoEnum getTipoApresentacao() {
		return tipoApresentacao;
	}

	public void setTipoApresentacao(TipoApresentacaoEnum tipoApresentacao) {
		this.tipoApresentacao = tipoApresentacao;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public BigDecimal getScale() {
		return scale;
	}

	public void setScale(BigDecimal scale) {
		this.scale = scale;
	}

	public BigInteger getWidth() {
		return width;
	}

	public void setWidth(BigInteger width) {
		this.width = width;
	}

	public BigInteger getHeight() {
		return height;
	}

	public void setHeight(BigInteger height) {
		this.height = height;
	}

	public BigInteger getAngle() {
		return angle;
	}

	public void setAngle(BigInteger angle) {
		this.angle = angle;
	}

	public BigInteger getX() {
		return x;
	}

	public void setX(BigInteger x) {
		this.x = x;
	}

	public BigInteger getY() {
		return y;
	}

	public void setY(BigInteger y) {
		this.y = y;
	}

	public String getTipoArquivo() {
		return tipoArquivo;
	}

	public void setTipoArquivo(String tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}

	public Boolean getAjuste() {
		return ajuste;
	}

	public void setAjuste(Boolean ajuste) {
		this.ajuste = ajuste;
	}

	public Boolean getCapa() {
		return capa;
	}

	public void setCapa(Boolean capa) {
		this.capa = capa;
	}
}
