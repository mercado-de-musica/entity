package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "interesses_de_compra_infos_gerais_produto_usuario")
public class InteressesDeCompraProduto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6575376658271013098L;
	
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@JoinColumn(name="id_interesse_de_compra")
	@OneToOne(fetch = FetchType.EAGER)
	private InteressesDeCompra interesseDeCompra;
	
	@JoinColumn(name="id_infos_gerais_produto_usuario")
	@OneToOne(fetch = FetchType.EAGER)
	private Produto infosGeraisProdutoUsuario;
	
	@Column(name="enviado")
	private Boolean enviado = Boolean.FALSE;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public InteressesDeCompra getInteresseDeCompra() {
		return interesseDeCompra;
	}

	public void setInteresseDeCompra(InteressesDeCompra interesseDeCompra) {
		this.interesseDeCompra = interesseDeCompra;
	}

	public Produto getProduto() {
		return infosGeraisProdutoUsuario;
	}

	public void setProduto(Produto infosGeraisProdutoUsuario) {
		this.infosGeraisProdutoUsuario = infosGeraisProdutoUsuario;
	}

	public Boolean getEnviado() {
		return enviado;
	}

	public void setEnviado(Boolean enviado) {
		this.enviado = enviado;
	}
}
