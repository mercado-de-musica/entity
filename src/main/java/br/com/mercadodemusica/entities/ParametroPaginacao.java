package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.mercadodemusica.enums.TipoDeParametroEnum;

@Entity
@Table(name = "parametro_paginacao", schema = "crawler")
public class ParametroPaginacao implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition = "serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Column(name = "url_pesquisa")
	private String urlPesquisa;

	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_de_parametro_enum")
	private TipoDeParametroEnum tipoDeParametroEnum;

	@ManyToOne
	@JoinColumn(name = "id_usuario", referencedColumnName = "id")
	private Usuario usuario;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getUrlPesquisa() {
		return urlPesquisa;
	}

	public void setUrlPesquisa(String urlPesquisa) {
		this.urlPesquisa = urlPesquisa;
	}

	public TipoDeParametroEnum getTipoDeParametroEnum() {
		return tipoDeParametroEnum;
	}

	public void setTipoDeParametroEnum(TipoDeParametroEnum tipoDeParametroEnum) {
		this.tipoDeParametroEnum = tipoDeParametroEnum;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
