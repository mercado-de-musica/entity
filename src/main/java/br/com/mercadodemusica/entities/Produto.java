package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "produto")
public class Produto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(nullable = false)
	private String nome;
	
	@Column(nullable = true, columnDefinition="text")
	private String descricao;
	
	@Column(nullable = false, name = "url_acesso", unique = true)
	private String urlAcesso;
	
	@Column(name="ativo", nullable = false)
	private Boolean ativo;
	
	@Column(name="produto_do_crawler", nullable = false)
	private Boolean produtoDoCrawler;
	
	@Column(name="produto_novo")
	private Boolean produtoNovo;
	
	@Column(name="data_insercao_produto", nullable = false)
	private Calendar dataInsercaoProduto;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
	private Usuario usuario;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getUrlAcesso() {
		return urlAcesso;
	}

	public void setUrlAcesso(String urlAcesso) {
		this.urlAcesso = urlAcesso;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Boolean getProdutoDoCrawler() {
		return produtoDoCrawler;
	}

	public void setProdutoDoCrawler(Boolean produtoDoCrawler) {
		this.produtoDoCrawler = produtoDoCrawler;
	}

	public Boolean getProdutoNovo() {
		return produtoNovo;
	}

	public void setProdutoNovo(Boolean produtoNovo) {
		this.produtoNovo = produtoNovo;
	}

	public Calendar getDataInsercaoProduto() {
		return dataInsercaoProduto;
	}

	public void setDataInsercaoProduto(Calendar dataInsercaoProduto) {
		this.dataInsercaoProduto = dataInsercaoProduto;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
