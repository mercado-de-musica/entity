package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "rastreamento_produto_correios")
public class RastreamentoProdutoCorreios extends Rastreamento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="codigo_plp_correio")
	private String codigoPlpCorreio;
	
	@Column(name="etiqueta_correio")
	private String etiquetaCorreio;
	
	@Column(name="digito_etiqueta_correio")
	private Integer digitoEtiquetaCorreio;
	
	@Column(name="valor_postagem_produto_correio", nullable = false)
	private BigDecimal valorPostagemProdutoCorreio;
		
	@OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "rastreamentoProdutoCorreios")
	@JsonManagedReference
	private List<StatusPostagemProdutoCorreio> statusPostagemProdutoCorreio;

	public String getCodigoPlpCorreio() {
		return codigoPlpCorreio;
	}

	public void setCodigoPlpCorreio(String codigoPlpCorreio) {
		this.codigoPlpCorreio = codigoPlpCorreio;
	}

	public String getEtiquetaCorreio() {
		return etiquetaCorreio;
	}

	public void setEtiquetaCorreio(String etiquetaCorreio) {
		this.etiquetaCorreio = etiquetaCorreio;
	}

	public Integer getDigitoEtiquetaCorreio() {
		return digitoEtiquetaCorreio;
	}

	public void setDigitoEtiquetaCorreio(Integer digitoEtiquetaCorreio) {
		this.digitoEtiquetaCorreio = digitoEtiquetaCorreio;
	}

	public BigDecimal getValorPostagemProdutoCorreio() {
		return valorPostagemProdutoCorreio;
	}

	public void setValorPostagemProdutoCorreio(BigDecimal valorPostagemProdutoCorreio) {
		this.valorPostagemProdutoCorreio = valorPostagemProdutoCorreio;
	}

	public List<StatusPostagemProdutoCorreio> getStatusPostagemProdutoCorreio() {
		return statusPostagemProdutoCorreio;
	}

	public void setStatusPostagemProdutoCorreio(List<StatusPostagemProdutoCorreio> statusPostagemProdutoCorreio) {
		this.statusPostagemProdutoCorreio = statusPostagemProdutoCorreio;
	}	
}
