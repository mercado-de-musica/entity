package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "rastreamento")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Rastreamento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4141760808945414614L;
	
	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@JoinColumn(name="id_infos_gerais_produto_usuario")
	@OneToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.EAGER)
	private Produto infosGeraisProdutoUsuario;
	
	@JoinColumn(name="id_comprador")
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Usuario comprador;
	
	@Column(name="finalizado")
	private Boolean finalizado = Boolean.FALSE;
	
	@Column(name="data_processamento")
	private Calendar dataProcessamento;
	
	@Column(name="desistencia", nullable = false)
	private Boolean desistencia = Boolean.FALSE;
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Produto getProduto() {
		return infosGeraisProdutoUsuario;
	}

	public void setProduto(Produto infosGeraisProdutoUsuario) {
		this.infosGeraisProdutoUsuario = infosGeraisProdutoUsuario;
	}

	public Usuario getComprador() {
		return comprador;
	}

	public void setComprador(Usuario comprador) {
		this.comprador = comprador;
	}

	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	public Calendar getDataProcessamento() {
		return dataProcessamento;
	}

	public void setDataProcessamento(Calendar dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public Boolean getDesistencia() {
		return desistencia;
	}

	public void setDesistencia(Boolean desistencia) {
		this.desistencia = desistencia;
	}
	
	
}
