package br.com.mercadodemusica.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.mercadodemusica.enums.StatusPostagemCorreiosEnum;

@Entity
@Table(name = "status_postagem_produto_correios")
public class StatusPostagemProdutoCorreio implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition="serial", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(name="status")
	@Enumerated(EnumType.STRING)
	private StatusPostagemCorreiosEnum status;
	
	@Column(name="data_do_processamento", nullable = false)
	private Calendar dataDoProcessamento;
	
	@Column(name="ativo", nullable = false)
	private Boolean ativo;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_rastreamento_produto_correios", referencedColumnName = "id", nullable=true, columnDefinition="bigint")
	@JsonBackReference
	private Rastreamento rastreamentoProdutoCorreios;
	

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}
	
	public StatusPostagemCorreiosEnum getStatus() {
		return status;
	}

	public void setStatus(StatusPostagemCorreiosEnum status) {
		this.status = status;
	}

	public Calendar getDataDoProcessamento() {
		return dataDoProcessamento;
	}

	public void setDataDoProcessamento(Calendar dataDoProcessamento) {
		this.dataDoProcessamento = dataDoProcessamento;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Rastreamento getRastreamentoProdutoCorreios() {
		return rastreamentoProdutoCorreios;
	}

	public void setRastreamentoProdutoCorreios(Rastreamento rastreamentoProdutoCorreios) {
		this.rastreamentoProdutoCorreios = rastreamentoProdutoCorreios;
	}
}
